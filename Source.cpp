// this is promgram game "CONNECT 4"
#include <iostream> // allows program for perform input and output
#include <cstdlib> // allows program for conversions of numbers to text

using namespace std; // allows program to perform namespace

int answer(); // function phototype
int table[7][6]; // function phototype
int player, choice; // variable player and choice to enter numbers 
int any = 0; // // variable any instead of number zero
bool final = false; // // variable of truth value

int answer() { // first function
	int i;
	cout << "Welcome to Connect 4" << endl; // say welcome
	do { // loop question if enter 1 to out loop
		cout << "Are you ready to play funny game ?\n==>(Yes=1,No=0)<==" << endl; // ask players
		cin >> i;
	} while (i == 0);
	cout << "Let's go ! ! ! " << endl; // when enter 1
	return i;
}

void check(int x) // loop check value of player
{
	if (table[x - 1][any] != 0 && any < 6)
	{
		any++;
		check(x);
	}
	else if (player == 1 && any < 6)
	{
		table[x - 1][any] = 1;
		any = 0;
	}
	else if (player == 2 && any < 6)
	{
		table[x - 1][any] = 2;
		any = 0;
	}
	else
	{
		cout << "Incorrect!" << endl;
		any = 0;
		player++;
	}
}

int pat() // function pattern game
{
	cout << "Please enter numbers (1-7)" << endl; // players can enter number 1-7
	for (int i = 0; i < 9; i++) // loop pattern
	{
		if (i < 2)
		{
			cout << " "; // space row
		}
		else if (i > 7)
		{
			cout << i - 1 << " " << endl; // space row
		}
		else {
			cout << i - 1 << "    "; // space row
		}
	}
	for (int i = 0; i < 6; i++) // loop table i=6,j=7
	{
		for (int j = 0; j < 7; j++)
		{
			if (table[j][i] != 0) //  Player1=X,Player2=O
			{
				if (table[j][i] == 1)
				{
					cout << "| X |"; // variable of player 1
				}
				else cout << "| O |"; // variable of player 1
			}
			else cout << "|   |"; // pattern game
		}
		cout << endl;
	}
	cout << "___________________________________" << endl; // end line
	return 0; // return value
}
	
int player_choice() // function choice player
{
	player = 1;
	while (final != true)
	{
		cout << "Player " << player << ": ";
		cin >> choice;
		// enter 1-7 else incorrect
		if (choice > 0 && choice < 8)
		{
			check(choice); // check variable of player
			pat();
			if (player == 1)
			{
				player++;
			}
			else
			{
				player--;
			}
		}
		else
		{
			cout << "Incorrect choice!" << endl;
		}
		
	}
	return 0; // return value
}

int main() // function main
{
	answer(); // function begin game
	pat(); // function pattern game
	player_choice(); // choice player

	system("PAUSE");
	return 0; // return value
} // end function main

